'use strict';

const winston = require('./winston');

function formatLog(options) {
  let formattedDate = formatDateLog(new Date());
  let message = options.meta && options.meta.message ? options.meta.message : options.message;
  return `${formattedDate} [${options.level.toUpperCase()}]\t--> ${message}`;
}

function formatDateLog(date) {
  return (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + '/' +
       ((date.getMonth() + 1) < 10 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1)) + '/' +
       date.getFullYear() + ' ' + 
       (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':' +
       (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':' +
       (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds()) + ':' + 
       (date.getMilliseconds() < 10 ? '00' + date.getMilliseconds() : date.getMilliseconds() < 100 ? '0' + date.getMilliseconds() : date.getMilliseconds());
}

const winstonAppLogger = new winston.Logger({
  transports: [
    new (winston.transports.Console)({
      timestamp: formatDateLog(new Date()),
      colorize: true,
      level: process.env.LOGGER_LEVEL
    }),
    new (require('winston-daily-rotate-file'))({
      filename: `${process.env.LOGGER_DIRECTORY}/_${process.env.LOGGER_FILENAME}`,
      json: false,
      formatter: formatLog,
      datePattern: 'yyyy-MM-dd',
      prepend: true,
      level: process.env.LOGGER_LEVEL
    })
  ]
});

const winstonRouterLogger = new winston.Logger({
  transports: [
    new (winston.transports.Console)({
      timestamp: formatDateLog(new Date()),
      colorize: true,
      level: process.env.ROUTER_LOGGER_LEVEL
    }),
    new (require('winston-daily-rotate-file'))({
      filename: `${process.env.LOGGER_DIRECTORY}/_${process.env.ROUTER_LOGGER_FILENAME}`,
      json: false,
      formatter: formatLog,
      datePattern: 'yyyy-MM-dd',
      prepend: true,
      level: process.env.ROUTER_LOGGER_LEVEL
    })
  ]
});

exports.appLogger = winstonAppLogger;
exports.routerLogger = winstonRouterLogger;
