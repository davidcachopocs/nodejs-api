'use strict';

const events = require('events');

const customEvents = new events.EventEmitter();

module.exports = customEvents;
