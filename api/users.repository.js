'use strict';

const _ = require('lodash'),
	model = require('./user.model');

const userNotFound = new Error('User not found');

let nextId = 0;
let users = [];

exports.save = (user, callback) => {
	model.checkUser(user, (error, validUser) => {
		if(error) {
			callback(error);
		}

		validUser.id = nextId++;
		users.push(validUser);
		callback(null, nextId);
	});
}

exports.update = (id, user, callback) => {
	model.checkUser(user, (error, validUser) => {
		if(error) {
			callback(error);
		}

		let index = _.findIndex(users, {id: id});
		if(!index > 0) {
			callback(userNotFound);
		}

		let userUpdated = {
			id: user.id,
			name: user.name,
			email: user.email
		}

		users.splice(index, 1, userUpdated);

		callback(null, userUpdated);
	});
}

exports.select = (query, callback) => {
	callback(null, _.filter(users, query));
}

exports.delete = (id, callback) => {
	model.checkUser(user, (error, validUser) => {
		if(error) {
			callback(error);
		}

		let index = _.findIndex(users, {id: id});
		if(!index > 0) {
			callback(userNotFound);
		}

		users.splice(index, 1);

		callback(null	);
	});
}